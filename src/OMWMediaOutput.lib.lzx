<?xml version="1.0" encoding="UTF-8"?>
<!--

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Open Multimedia Web Suite.

Open Multimedia Web Suite is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Open Multimedia Web Suite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Open Multimedia Web Suite.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.
For all other info read README.

-->

<library>
	<!-- Define an abstract media output, this class should be inerithed by all media aoutput for example audioOutput and videoOutput-->
	<class name="OMWMediaOutput">
		<attribute name="url"		type="string"	value=""		/><!-- The Media Url -->
		<attribute name="progress"	type="number"	value="0"		/><!-- The Media loading progress 				@readonly	-->
		<attribute name="stream"					value="null"	/><!-- The Stream used to load the media -->
		<attribute name="volume"	type="number"	value="1"		/><!-- The volume to play media -->
		<attribute name="paused"	type="boolean"	value="true"	/><!-- Tell if media is paused or not			@readonly	-->
		<attribute name="playing"	type="boolean"	value="false"	/><!-- Tell if media is playing or not			@readonly	-->
		<attribute name="totaltime"	type="number"	value="0"	  	/><!-- The time needed to play all media		@readonly	-->
		<attribute name="time"		type="number"	value="0"		/><!-- The actual playing time					@readonly	-->

		<handler name="onvolume">
			//gDebug(this, "onvolume");
			setVolume(volume);
		</handler>
		
		<event name="onerror"/>
		<event name="onstop" />
		
		<!-- Pause the playing of media @virtual	-->
		<method name="pause">
			//gDebug(this, "pause()");
		</method>
		<!-- Start the playing of media @virtual	-->
		<method name="play" args="time=null,rel:Boolean = false" returns="void"> 
			//gDebug(this, "play()");
		</method>
		<!-- Stop the playing of media	@virtual	-->
        <method name="stop" args="time=null,rel:Boolean = false" returns="void">
			//gDebug(this, "stop()");
		</method>
		<!-- Seek the media to the given time/frame @virtual -->
		<method name="seek" args="absTime:Number" returns="void">
			//gDebug(this, "seek()", absTime);
		</method>
		<!-- Make the browser download the media	@virtual -->
		<method name="downloadMedia">
			//gDebug(this, "downloadMedia");
		</method>
		<!-- Set the volume of playing media		@virtual -->
		<method name="setVolume" args="vol:Number" returns="void">
			//gDebug(this, "setVolume", vol);
		</method>
		
	</class>
	
	<include href="macros.lzx"/>
</library> 
